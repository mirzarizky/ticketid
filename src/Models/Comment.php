<?php

namespace Mirzarizky\Ticketid\Models;

use Illuminate\Database\Eloquent\Model;
use Mirzarizky\Ticketid\Traits\ContentEllipse;
use Mirzarizky\Ticketid\Traits\Purifiable;

class Comment extends Model
{
    use ContentEllipse;
    use Purifiable;

    protected $table = 'ticketid_comments';

    /**
     * Get related ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo('Mirzarizky\Ticketid\Models\Ticket', 'ticket_id');
    }

    /**
     * Get comment owner.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function attachments()
    {
        return $this->hasMany('Mirzarizky\Ticketid\Models\Attachment', 'comment_id');
    }
}
