<?php

namespace Mirzarizky\Ticketid\Models;

use Illuminate\Database\Eloquent\Model;

class Telegram extends Model {

    protected $table = 'ticketid_telegrams';

    protected $fillable = ['user_id', 'username', 'chat_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}