<?php

namespace Mirzarizky\Ticketid\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model {

    protected $table = 'ticketid_attachments';

    /**
     * Get related ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comment() {
        return $this->belongsTo('Mirzarizky\Ticketid\Models\Comment', 'comment_id');
    }

    public function ticket() {
        return $this->belongsto('Mirzarizky\Ticketid\Models\Ticket', 'ticket_id');
    }
    /**
     * Get comment owner
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}