<?php

namespace Mirzarizky\Ticketid\Middleware;

use Closure;
use Mirzarizky\Ticketid\Models\Agent;

class IsAgentMiddleware
{
    /**
     * Run the request filter.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Agent::isAgent() || Agent::isAdmin()) {
            return $next($request);
        }

        return redirect()->action('\Mirzarizky\Ticketid\Controllers\TicketsController@index')
            ->with('warning', trans('ticketid::lang.you-are-not-permitted-to-access'));
    }
}
