<nav>
    <ul class="nav nav-pills">
        <li role="presentation" class="nav-item">
            <a class="nav-link {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\TicketsController@index')) ? "active" : "" !!}"
               href="{{ action('\Mirzarizky\Ticketid\Controllers\TicketsController@index') }}">{{ trans('ticketid::lang.nav-active-tickets') }}
                <span class="badge badge-pill badge-secondary ">
                     <?php
                    if ($u->isAdmin()) {
                        echo Mirzarizky\Ticketid\Models\Ticket::active()->count();
                    } elseif ($u->isAgent()) {
                        echo Mirzarizky\Ticketid\Models\Ticket::active()->agentUserTickets($u->id)->count();
                    } else {
                        echo Mirzarizky\Ticketid\Models\Ticket::userTickets($u->id)->active()->count();
                    }
                    ?>
                </span>
            </a>
        </li>
        <li role="presentation" class="nav-item">
            <a class="nav-link {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\TicketsController@indexComplete')) ? "active" : "" !!}"
               href="{{ action('\Mirzarizky\Ticketid\Controllers\TicketsController@indexComplete') }}">{{ trans('ticketid::lang.nav-completed-tickets') }}
                <span class="badge badge-pill badge-secondary">
                    <?php
                    if ($u->isAdmin()) {
                        echo Mirzarizky\Ticketid\Models\Ticket::complete()->count();
                    } elseif ($u->isAgent()) {
                        echo Mirzarizky\Ticketid\Models\Ticket::complete()->agentUserTickets($u->id)->count();
                    } else {
                        echo Mirzarizky\Ticketid\Models\Ticket::userTickets($u->id)->complete()->count();
                    }
                    ?>
                </span>
            </a>
        </li>

        @if($u->isAdmin())
            <li role="presentation" class="nav-item">
                <a class="nav-link {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\DashboardController@index')) || Request::is($setting->grab('admin_route').'/indicator*') ? "active" : "" !!}"
                   href="{{ action('\Mirzarizky\Ticketid\Controllers\DashboardController@index') }}">{{ trans('ticketid::admin.nav-dashboard') }}</a>
            </li>

            <li role="presentation" class="nav-item dropdown">

                <a class="nav-link dropdown-toggle {!!
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\StatusesController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\PrioritiesController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\AgentsController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\CategoriesController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\ConfigurationsController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\AdministratorsController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\TelegramsController@index').'*')
                    ? "active" : "" !!}"
                   data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    {{ trans('ticketid::admin.nav-settings') }}
                </a>
                <div class="dropdown-menu">
                    <a  class="dropdown-item {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\StatusesController@index').'*') ? "active" : "" !!}"
                        href="{{ action('\Mirzarizky\Ticketid\Controllers\StatusesController@index') }}">{{ trans('ticketid::admin.nav-statuses') }}</a>

                    <a  class="dropdown-item {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\PrioritiesController@index').'*') ? "active" : "" !!}"
                        href="{{ action('\Mirzarizky\Ticketid\Controllers\PrioritiesController@index') }}">{{ trans('ticketid::admin.nav-priorities') }}</a>

                    <a  class="dropdown-item {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\AgentsController@index').'*') ? "active" : "" !!}"
                        href="{{ action('\Mirzarizky\Ticketid\Controllers\AgentsController@index') }}">{{ trans('ticketid::admin.nav-agents') }}</a>

                    <a  class="dropdown-item {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\CategoriesController@index').'*') ? "active" : "" !!}"
                        href="{{ action('\Mirzarizky\Ticketid\Controllers\CategoriesController@index') }}">{{ trans('ticketid::admin.nav-categories') }}</a>

                    <a  class="dropdown-item {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\ConfigurationsController@index').'*') ? "active" : "" !!}"
                        href="{{ action('\Mirzarizky\Ticketid\Controllers\ConfigurationsController@index') }}">{{ trans('ticketid::admin.nav-configuration') }}</a>

                    <a  class="dropdown-item {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\AdministratorsController@index').'*') ? "active" : "" !!}"
                        href="{{ action('\Mirzarizky\Ticketid\Controllers\AdministratorsController@index')}}">{{ trans('ticketid::admin.nav-administrator') }}</a>

                    <a  class="dropdown-item {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\TelegramsController@index').'*') ? "active" : "" !!}"
                        href="{{ action('\Mirzarizky\Ticketid\Controllers\TelegramsController@index')}}">{{ trans('ticketid::admin.nav-telegram') }}</a>

                </div>
            </li>
        @endif
        @if($u->isAgent())
            <li role="presentation" class="nav-item">
                <a class="nav-link {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\DashboardController@index')) || Request::is($setting->grab('admin_route').'/indicator*') ? "active" : "" !!}"
                   href="{{ action('\Mirzarizky\Ticketid\Controllers\DashboardController@index') }}">{{ trans('ticketid::admin.nav-dashboard') }}</a>
            </li>

            <li role="presentation" class="nav-item dropdown">

                <a class="nav-link dropdown-toggle {!!
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\TelegramsController@index').'*')
                    ? "active" : "" !!}"
                   data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    {{--{{ trans('ticketid::admin.nav-settings') }}--}}
                    Configuration
                </a>
                <div class="dropdown-menu">
                    <a  class="dropdown-item {!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\TelegramsController@index').'*') ? "active" : "" !!}"
                        href="{{ route($setting->grab('main_route').'.agent.telegram.index')}}">{{ trans('ticketid::admin.nav-telegram') }}</a>
                </div>
            </li>
        @endif

    </ul>
</nav>