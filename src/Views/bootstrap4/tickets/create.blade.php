@extends('ticketid::layouts.master')
@section('page', trans('ticketid::lang.create-ticket-title'))
@section('page_title', trans('ticketid::lang.create-new-ticket'))

@section('ticketid_content')
    {!! CollectiveForm::open([
                    'route'=>$setting->grab('main_route').'.store',
                    'method' => 'POST',
                    'files' => true
                    ]) !!}
    <div class="form-group row">
        {!! CollectiveForm::label('subject', trans('ticketid::lang.subject') . trans('ticketid::lang.colon'), ['class' => 'col-lg-2 col-form-label']) !!}
        <div class="col-lg-10">
            {!! CollectiveForm::text('subject', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="form-text text-muted">{!! trans('ticketid::lang.create-ticket-brief-issue') !!}</small>
        </div>
    </div>
    <div class="form-group row">
        {!! CollectiveForm::label('content', trans('ticketid::lang.description') . trans('ticketid::lang.colon'), ['class' => 'col-lg-2 col-form-label']) !!}
        <div class="col-lg-10">
            {!! CollectiveForm::textarea('content', null, ['class' => 'form-control summernote-editor', 'rows' => '5', 'required' => 'required']) !!}
            <small class="form-text text-muted">{!! trans('ticketid::lang.create-ticket-describe-issue') !!}</small>
        </div>
    </div>
    <div class="form-row mt-5">
        <div class="form-group col-lg-6 row">
            {!! CollectiveForm::label('priority', trans('ticketid::lang.priority') . trans('ticketid::lang.colon'), ['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! CollectiveForm::select('priority_id', $priorities, null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group col-lg-6 row">
            {!! CollectiveForm::label('category', trans('ticketid::lang.category') . trans('ticketid::lang.colon'), ['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! CollectiveForm::select('category_id', $categories, null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        {!! CollectiveForm::hidden('agent_id', 'auto') !!}
    </div>
    <div class="form-group row">
        {!! CollectiveForm::label('file_upload', trans('ticketid::lang.file-upload') . trans('ticketid::lang.colon'), ['class' => 'col-lg-2']) !!}
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-10" id="file-upload">
                    {!! CollectiveForm::file('file_upload[]', ['class' => 'my-1 p-auto form-control'], null) !!}
                </div>
                <div class="col-lg-2" id="plus-minus">
                    <a class="btn btn-primary my-2" id="more-file"><span class="fa fa-plus"></span></a>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="form-group row">
        <div class="col-lg-10 offset-lg-2">
            {!! link_to_route($setting->grab('main_route').'.index', trans('ticketid::lang.btn-back'), null, ['class' => 'btn btn-link']) !!}
            {!! CollectiveForm::submit(trans('ticketid::lang.btn-submit'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
    {!! CollectiveForm::close() !!}
@endsection

@section('footer')
    @include('ticketid::tickets.partials.summernote')
    <script type="text/javascript">
        var x = 1;
        $('#more-file').click(function(e) {
            e.preventDefault();
            if(x < {!! $setting->grab('max_upload_files') !!}) {
                x++;
                $('#file-upload').append(`<input type="file" id="form-file${x}" name="file_upload[]" class="my-1 p-auto form-control"/>`);
                if(x == 2) {
                    $('#plus-minus').append(`<a class="btn btn-danger m-2" id="remove-form"><span class="fa fa-trash"></span></a>`);
                }
            }
            else {
                alert('You Reached the limits');
            }
        });

        $('#plus-minus').on("click","#remove-form",function(e) {
            e.preventDefault();
            if(x == 2) {
                $('#form-file'+x).remove();
                $('#remove-form').remove();
            } else {
                $('#form-file'+x).remove();
            }
            x--;
        });
    </script>
@append