{!! CollectiveForm::open(['method' => 'POST', 'route' => $setting->grab('main_route').'-comment.store','files' => true, 'class' => '']) !!}


{!! CollectiveForm::hidden('ticket_id', $ticket->id ) !!}


{!! CollectiveForm::textarea('content', null, ['class' => 'form-control summernote-editor', 'rows' => "3"]) !!}
<div class="row mt-2">
    <div class="col-lg-2 col-md-1">
        {!! CollectiveForm::label('file_upload', trans('ticketid::lang.file-upload') . trans('ticketid::lang.colon'), ['class' => 'col-lg-12']) !!}
    </div>
    <div class="col-lg-10 col-md-11 row">
        <div class="col-lg-10" id="file-upload">
            {!! CollectiveForm::file('file_upload[]', ['class' => 'form-control my-1 p-auto'], null) !!}
        </div>
        <div class="col-lg-2" id="plus-minus">
            <a class="btn btn-primary my-2" id="more-file"><span class="fa fa-plus"></span></a>
        </div>
    </div>
</div>
{!! CollectiveForm::submit( trans('ticketid::lang.reply'), ['class' => 'btn btn-outline-primary pull-right mt-3 mb-3']) !!}

{!! CollectiveForm::close() !!}
    