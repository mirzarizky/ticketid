@if(!$comments->isEmpty())
    @foreach($comments as $comment)
        @if($u->isAdmin() || $u->isAgent())
            <div class="card mb-3 {!! $comment->isPinned ? "border-dark" : ""!!} {!! $comment->user->tickets_role ? "border-info" : "" !!}">
                <div class="card-header d-flex {!! $comment->isPinned ? "border-dark" : ""!!} justify-content-between align-items-baseline flex-wrap {!! $comment->user->tickets_role ? "bg-info text-white" : "" !!}">
                    <div>{!! $comment->user->name !!}</div>
                    <div>{!! $comment->created_at->diffForHumans() !!}</div>
                    @if($comment->isPinned)
                        <div><a class="btn btn-danger" href="{!! route($setting->grab('main_route').'.unpin',  ['id' => $comment->id]) !!}">Unpin <i class="fa fa-flag-checkered"></i></a></div>
                    @else
                        <div><a class="btn btn-primary" href="{!! route($setting->grab('main_route').'.pin',  ['id' => $comment->id]) !!}">{!! trans('ticketid::lang.pin') !!} <i class="fa fa-flag"></i></a></div>
                    @endif
                </div>
                <div class="card-body pb-0">
                    {!! $comment->html !!}
                </div>
                @if(!$comment->attachments->isEmpty())
                    <div class="card-footer {!! $comment->isPinned ? "border-dark" : ""!!} py-0">
                        <div class="row">
                            <div class="col-md-6">
                                <strong>Attachments ({{$comment->attachments->count()}})</strong>
                                <ul class="my-0">
                                    @foreach($comment->attachments as $comattachment)
                                        <li class="my-0">
                                            {!! link_to_route($setting->grab('main_route').'.getattachment', $comattachment->original_filename, $comattachment->id)!!}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        @else
            <div class="card mb-3 {!! $comment->user->tickets_role ? "border-info" : "" !!}">
                <div class="card-header d-flex justify-content-between align-items-baseline flex-wrap {!! $comment->user->tickets_role ? "bg-info text-white" : "" !!}">
                    <div>{!! $comment->user->name !!}</div>
                    <div>{!! $comment->created_at->diffForHumans() !!}</div>
                </div>
                <div class="card-body pb-0">
                    {!! $comment->html !!}
                </div>
                @if(!$comment->attachments->isEmpty())
                    <div class="card-footer py-0">
                        <div class="row">
                            <div class="col-md-6">
                                <strong>Attachments ({{$comment->attachments->count()}})</strong>
                                <ul class="my-0">
                                    @foreach($comment->attachments as $comattachment)
                                        <li class="my-0">
                                            {!! link_to_route($setting->grab('main_route').'.getattachment', $comattachment->original_filename, $comattachment->id)!!}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        @endif
    @endforeach
@endif