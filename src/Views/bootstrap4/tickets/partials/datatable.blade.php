<table class="table table-condensed table-stripe ddt-responsive" class="ticketid-table">
    <thead>
        <tr>
            <td>{{ trans('ticketid::lang.table-id') }}</td>
            <td>{{ trans('ticketid::lang.table-subject') }}</td>
            <td>{{ trans('ticketid::lang.table-status') }}</td>
            <td>{{ trans('ticketid::lang.table-last-updated') }}</td>
            <td>{{ trans('ticketid::lang.table-agent') }}</td>
          @if( $u->isAgent() || $u->isAdmin() )
            <td>{{ trans('ticketid::lang.table-priority') }}</td>
            <td>{{ trans('ticketid::lang.table-owner') }}</td>
            <td>{{ trans('ticketid::lang.table-category') }}</td>
          @endif
        </tr>
    </thead>
</table>