<div class="card mb-3">
    <div class="card-body row">
        <div class="col-md-6">
            <p><strong>{{ trans('ticketid::lang.owner') }}</strong>{{ trans('ticketid::lang.colon') }}{{ $ticket->user_id == $u->id ? $u->name : $ticket->user->name }}</p>
            <p>
                <strong>{{ trans('ticketid::lang.status') }}</strong>{{ trans('ticketid::lang.colon') }}
                @if( $ticket->isComplete() && ! $setting->grab('default_close_status_id') )
                    <span style="color: blue">Complete</span>
                @else
                    <span style="color: {{ $ticket->status->color }}">{{ $ticket->status->name }}</span>
                @endif

            </p>
            <p>
                <strong>{{ trans('ticketid::lang.priority') }}</strong>{{ trans('ticketid::lang.colon') }}
                <span style="color: {{ $ticket->priority->color }}">
                    {{ $ticket->priority->name }}
                </span>
            </p>
        </div>
        <div class="col-md-6">
            <p> <strong>{{ trans('ticketid::lang.responsible') }}</strong>{{ trans('ticketid::lang.colon') }}{{ $ticket->agent_id == $u->id ? $u->name : $ticket->agent->name }}</p>
            <p>
                <strong>{{ trans('ticketid::lang.category') }}</strong>{{ trans('ticketid::lang.colon') }}
                <span style="color: {{ $ticket->category->color }}">
                    {{ $ticket->category->name }}
                </span>
            </p>
            <p> <strong>{{ trans('ticketid::lang.created') }}</strong>{{ trans('ticketid::lang.colon') }}{{ $ticket->created_at->diffForHumans() }}</p>
            <p> <strong>{{ trans('ticketid::lang.last-update') }}</strong>{{ trans('ticketid::lang.colon') }}{{ $ticket->updated_at->diffForHumans() }}</p>
        </div>
    </div>
    @if(!$ticket->attachments->isEmpty())
    <div class="card-footer py-0">
        <div class="row">
            <div class="col-md-6">
                <strong>Ticket Attachments ({{$ticket->attachments->count()}})</strong>
                    <ul class="my-0">
                        @foreach($ticket->attachments as $attachment)
                            <li class="my-0">
                                {!! link_to_route($setting->grab('main_route').'.getattachment', $attachment->original_filename, $attachment->id)!!}
                            </li>
                        @endforeach
                    </ul>
            </div>
        </div>
    </div>
    @endif
</div>

{!! $ticket->html !!}

{!! CollectiveForm::open([
                'method' => 'DELETE',
                'route' => [
                            $setting->grab('main_route').'.destroy',
                            $ticket->id
                            ],
                'id' => "delete-ticket-$ticket->id"
                ])
!!}
{!! CollectiveForm::close() !!}


@if($u->isAgent() || $u->isAdmin())
    @include('ticketid::tickets.edit')
@endif

{{-- // OR; Modal Window: 2/2 --}}
@if($u->isAdmin())
    @include('ticketid::tickets.partials.modal-delete-confirm')
@endif
{{-- // END Modal Window: 2/2 --}}
