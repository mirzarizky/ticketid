@extends('ticketid::layouts.master')

@section('page', 'Telegram Settings')

@section('ticketid_content_parent_class', 'p-0')

@section('ticketid_content')
    <ul class="nav nav-tabs nav-justified">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#init">{{ trans('ticketid::admin.config-index-initial') }}</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#telegram-user">Telegram User</a></li>
    </ul>

    <div class="tab-content">
        <div id="init" class="tab-pane fade show active">
            @include('ticketid::admin.telegram.tables.init_')
        </div>
        <div id="telegram-user" class="tab-pane fade">
            @if ($telegramUsers->isEmpty())
                <h3 class="text-center">There are not users with telegram.
                </h3>
            @else
                @include('ticketid::admin.telegram.tables.user_')
            @endif
        </div>
    </div>

@stop

@section('footer')
@append