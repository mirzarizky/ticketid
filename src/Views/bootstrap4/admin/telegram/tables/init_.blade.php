<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th class="text-center">{{ trans('ticketid::admin.table-hash') }}</th>
                <th class="text-center">Name</th>
                <th class="text-center">Value</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">1</td>
                <td>Telegram Notification Status</td>
                <td>
                    @if($telegramSettings->value == true)
                        Enabled
                    @else
                        Disabled
                    @endif
                </td>
                <td class="text-center">
                    @if($telegramSettings->value == true)
                        <a class="btn btn-danger" href="{!! route($setting->grab('admin_route').'.configuration.edit', [$telegramSettings->id]) !!}" title="{{ trans('ticketid::admin.table-edit').' '.$telegramSettings->slug }}" data-toggle="tooltip">
                            Disable
                        </a>
                    @else
                        <a class="btn btn-primary" href="{!! route($setting->grab('admin_route').'.configuration.edit', [$telegramSettings->id]) !!}" title="{{ trans('ticketid::admin.table-edit').' '.$telegramSettings->slug }}" data-toggle="tooltip">
                            Enable
                        </a>
                    @endif
                </td>
            </tr><tr>
                <td class="text-center">2</td>
                <td>Telegram Bot Username</td>
                <td>
                    @if($botUsername->value == 'ticketidbot')
                        Your bot username still default : {!! '@'.$botUsername->value !!}
                    @else
                        {!! '@'.$botUsername->value !!}
                    @endif
                </td>
                <td class="text-center">
                    <a class="btn btn-primary" href="{!! route($setting->grab('admin_route').'.configuration.edit', [$botUsername->id]) !!}" title="{{ trans('ticketid::admin.table-edit').' '.$botUsername->slug }}" data-toggle="tooltip">
                        Change
                    </a>
                </td>
            </tr>
            <tr>
                <td class="text-center">3</td>
                <td>Telegram Webhook Status</td>
                <td>
                    @if(empty($webhookStatus['url']))
                        Webhook was empty.
                    @else
                        Webhook was set to
                        {!! $webhookStatus['url'] !!}
                    @endif
                </td>
                <td class="text-center">
                    @if(empty($webhookStatus['url']))
                        <a class="btn btn-primary" data-toggle="modal" data-target="#modal-setwebhook" href="#">
                            Set Webhook
                        </a>
                    @else
                        <a class="btn btn-danger" href="{!! route($setting->grab('admin_route').'.telegram.removewebhook') !!}" onclick="event.preventDefault();
                        document.getElementById('removewebhook-form').submit();">
                            Remove Webhook
                        </a>

                        <form id="removewebhook-form" action="{!! route($setting->grab('admin_route').'.telegram.removewebhook') !!}" method="POST" style="display: none;">
                            {{csrf_field()}}
                        </form>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    @if(empty($webhookStatus['url']))
        <div class="modal fade" id="modal-setwebhook" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form method="POST" action="{!! route($setting->grab('admin_route').'.telegram.setwebhook') !!}" accept-charset="UTF-8" class="form-horizontal">
                        <input name="_method" type="hidden" value="POST">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <h5 class="modal-title">Set Webhook</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="url" class="">URL: </label>
                                <input class="form-control" id="url" type="url" name="url" placeholder="https://" required>

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input class="btn btn-primary" type="submit" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

</div>
