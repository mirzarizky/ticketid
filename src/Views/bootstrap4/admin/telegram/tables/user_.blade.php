<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th class="text-center">{{ trans('ticketid::admin.table-hash') }}</th>
            <th class="text-center">Name</th>
            <th class="text-center">Telegram Username</th>
            <th class="text-center">Status</th>
            {{--<th class="text-center">Action</th>--}}
        </tr>
        </thead>
        <tbody>
            @foreach($telegramUsers as $user)
                <tr>
                    <td class="text-center">{!! $user->id !!}</td>
                    <td>{!! $user->name !!}</td>
                    <td>
                        {!! $user->telegram->username !!}
                    </td>
                    <td>
                        @if(empty($user->telegram->chat_id))
                            Not Activated
                        @else
                            Activated
                        @endif
                    </td>
                    {{--<td class="text-center">--}}
                        {{--BTN--}}
                    {{--</td>--}}
                </tr>
            @endforeach
        </tbody>
    </table>

</div>