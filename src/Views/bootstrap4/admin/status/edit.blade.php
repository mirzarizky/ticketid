@extends('ticketid::layouts.master')
@section('page', trans('ticketid::admin.status-edit-title', ['name' => ucwords($status->name)]))

@section('ticketid_content')
    {!! CollectiveForm::model($status, [
                                    'route' => [$setting->grab('admin_route').'.status.update', $status->id],
                                    'method' => 'PATCH'
                                    ]) !!}
        @include('ticketid::admin.status.form', ['update', true])
    {!! CollectiveForm::close() !!}
@stop
