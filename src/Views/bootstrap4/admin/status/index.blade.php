@extends('ticketid::layouts.master')

@section('page', trans('ticketid::admin.status-index-title'))

@section('ticketid_header')
{!! link_to_route(
    $setting->grab('admin_route').'.status.create',
    trans('ticketid::admin.btn-create-new-status'), null,
    ['class' => 'btn btn-primary'])
!!}
@stop

@section('ticketid_content_parent_class', 'p-0')

@section('ticketid_content')
    @if ($statuses->isEmpty())
        <h3 class="text-center">{{ trans('ticketid::admin.status-index-no-statuses') }}
            {!! link_to_route($setting->grab('admin_route').'.status.create', trans('ticketid::admin.status-index-create-new')) !!}
        </h3>
    @else
        <div id="message"></div>
        <table class="table table-hover mb-0">
            <thead>
                <tr>
                    <th>{{ trans('ticketid::admin.table-id') }}</th>
                    <th>{{ trans('ticketid::admin.table-name') }}</th>
                    <th>{{ trans('ticketid::admin.table-action') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($statuses as $status)
                <tr>
                    <td style="vertical-align: middle">
                        {{ $status->id }}
                    </td>
                    <td style="color: {{ $status->color }}; vertical-align: middle">
                        {{ $status->name }}
                    </td>
                    <td>
                        {!! link_to_route(
                                                $setting->grab('admin_route').'.status.edit', trans('ticketid::admin.btn-edit'), $status->id,
                                                ['class' => 'btn btn-info'] )
                            !!}

                            {!! link_to_route(
                                                $setting->grab('admin_route').'.status.destroy', trans('ticketid::admin.btn-delete'), $status->id,
                                                [
                                                'class' => 'btn btn-danger deleteit',
                                                'form' => "delete-$status->id",
                                                "node" => $status->name
                                                ])
                            !!}
                        {!! CollectiveForm::open([
                                        'method' => 'DELETE',
                                        'route' => [
                                                    $setting->grab('admin_route').'.status.destroy',
                                                    $status->id
                                                    ],
                                        'id' => "delete-$status->id"
                                        ])
                        !!}
                        {!! CollectiveForm::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

@stop

@section('footer')
    <script>
        $( ".deleteit" ).click(function( event ) {
            event.preventDefault();
            if (confirm("{!! trans('ticketid::admin.status-index-js-delete') !!}" + $(this).attr("node") + " ?"))
            {
                $form = $(this).attr("form");
                $("#" + $form).submit();
            }

        });
    </script>
@append
