@extends('ticketid::layouts.master')
@section('page', trans('ticketid::admin.status-create-title'))

@section('ticketid_content')
    {!! CollectiveForm::open(['route'=> $setting->grab('admin_route').'.status.store', 'method' => 'POST', 'class' => '']) !!}
        @include('ticketid::admin.status.form')
    {!! CollectiveForm::close() !!}
@stop
