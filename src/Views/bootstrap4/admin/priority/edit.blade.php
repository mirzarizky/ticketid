@extends('ticketid::layouts.master')
@section('page', trans('ticketid::admin.priority-edit-title', ['name' => ucwords($priority->name)]))

@section('ticketid_content')
    {!! CollectiveForm::model($priority, [
                                'route' => [$setting->grab('admin_route').'.priority.update', $priority->id],
                                'method' => 'PATCH'
                                ]) !!}
        @include('ticketid::admin.priority.form', ['update', true])
    {!! CollectiveForm::close() !!}
@stop
