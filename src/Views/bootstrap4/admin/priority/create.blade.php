@extends('ticketid::layouts.master')
@section('page', trans('ticketid::admin.priority-create-title'))

@section('ticketid_content')
    {!! CollectiveForm::open(['route'=> $setting->grab('admin_route').'.priority.store', 'method' => 'POST', 'class' => '']) !!}
        @include('ticketid::admin.priority.form')
    {!! CollectiveForm::close() !!}
@stop
