@extends('ticketid::layouts.master')
@section('page', trans('ticketid::admin.category-edit-title', ['name' => ucwords($category->name)]))

@section('ticketid_content')
    {!! CollectiveForm::model($category, [
                                'route' => [$setting->grab('admin_route').'.category.update', $category->id],
                                'method' => 'PATCH',
                                'class' => ''
                                ]) !!}
        @include('ticketid::admin.category.form', ['update', true])
    {!! CollectiveForm::close() !!}
@stop
