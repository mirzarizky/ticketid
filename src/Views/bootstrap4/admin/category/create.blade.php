@extends('ticketid::layouts.master')
@section('page', trans('ticketid::admin.category-create-title'))

@section('ticketid_content')
    {!! CollectiveForm::open(['route'=> $setting->grab('admin_route').'.category.store', 'method' => 'POST', 'class' => '']) !!}
        @include('ticketid::admin.category.form')
    {!! CollectiveForm::close() !!}
@stop
