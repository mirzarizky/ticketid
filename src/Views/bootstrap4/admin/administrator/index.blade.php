@extends('ticketid::layouts.master')

@section('page', trans('ticketid::admin.administrator-index-title'))

@section('ticketid_header')
{!! link_to_route(
    $setting->grab('admin_route').'.administrator.create',
    trans('ticketid::admin.btn-create-new-administrator'), null,
    ['class' => 'btn btn-primary'])
!!}
@stop

@section('ticketid_content_parent_class', 'p-0')

@section('ticketid_content')
    @if ($administrators->isEmpty())
        <h3 class="text-center">{{ trans('ticketid::admin.administrator-index-no-administrators') }}
            {!! link_to_route($setting->grab('admin_route').'.administrator.create', trans('ticketid::admin.administrator-index-create-new')) !!}
        </h3>
    @else
        <div id="message"></div>
        <table class="table table-hover mb-0">
            <thead>
            <tr>
                <th>{{ trans('ticketid::admin.table-id') }}</th>
                <th>{{ trans('ticketid::admin.table-name') }}</th>
                <th>{{ trans('ticketid::admin.table-remove-administrator') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($administrators as $administrator)
                <tr>
                    <td>
                        {{ $administrator->id }}
                    </td>
                    <td>
                        {{ $administrator->name }}
                    </td>
                    <td>
                        {!! CollectiveForm::open([
                        'method' => 'DELETE',
                        'route' => [
                                    $setting->grab('admin_route').'.administrator.destroy',
                                    $administrator->id
                                    ],
                        'id' => "delete-$administrator->id"
                        ]) !!}
                        {!! CollectiveForm::submit(trans('ticketid::admin.btn-remove'), ['class' => 'btn btn-danger']) !!}
                        {!! CollectiveForm::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    @endif

@stop
