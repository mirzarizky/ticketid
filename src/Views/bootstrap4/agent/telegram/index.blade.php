@extends('ticketid::layouts.master')

@section('page', 'Telegram Configs')

@section('ticketid_content_parent_class', 'p-0')

@section('ticketid_content')
    @if(empty($user->telegram_id))
        <h3 class="text-center">
            Activate your telegram to get notified. <br/><a class="btn btn-md btn-primary mt-2" data-toggle="modal" data-target="#modal-activate" href="#">Activate</a>
        </h3>
        <div class="modal fade" id="modal-activate" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg modal-md" role="document">
                <div class="modal-content">

                    <form method="POST" action="{!! route($setting->grab('main_route').'.agent.telegram.activation') !!}" accept-charset="UTF-8" class="form-horizontal">
                        <input name="_method" type="hidden" value="POST">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <h5 class="modal-title">Activate Telegram</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Add  {!! '@'.$setting->grab('telegram_bot_username') !!} then click start. and type '/activate'.
                            </p>
                            <div class="form-group">
                                <label for="username" class="">Username: </label>
                                <input class="form-control" id="username" type="text" name="username" placeholder="yourusername" required>
                            </div>
                            <div class="form-group">
                                <label for="secret_pin" class="">Secret PIN: </label>
                                <input class="form-control" id="secret_pin" type="number" name="secret_pin" placeholder="888999" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input class="btn btn-primary" type="submit" value="Submit">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @else
        <h3 class="text-center">
            Your telegram has been activated. <br/><a class="btn btn-md btn-danger my-2" data-toggle="modal" data-target="#modal-deactivate" href="#">Deactivate</a>
        </h3>
        <div class="modal fade" id="modal-deactivate" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg modal-md" role="document">
                <div class="modal-content">

                    <form method="POST" action="{!! route($setting->grab('main_route').'.agent.telegram.deactivate') !!}" accept-charset="UTF-8" class="form-horizontal">
                        <input name="_method" type="hidden" value="POST">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <h5 class="modal-title">Deactivate Telegram</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="username" class="">Username: </label>
                                <input class="form-control" id="username" type="text" name="username" placeholder="type your username to deactivate Telegram Notification" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input class="btn btn-primary" type="submit" value="Submit">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

@stop
