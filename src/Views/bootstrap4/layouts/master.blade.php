@extends($master)

@section('content')
    @include('ticketid::shared.header')

    <div class="container">
        <div class="card mb-3">
            <div class="card-body">
                @include('ticketid::shared.nav')
            </div>
        </div>
        @if(View::hasSection('ticketid_content'))
            <div class="card">
                <h5 class="card-header d-flex justify-content-between align-items-baseline flex-wrap">
                    @if(View::hasSection('page_title'))
                        <span>@yield('page_title')</span>
                    @else
                        <span>@yield('page')</span>
                    @endif

                    @yield('ticketid_header')
                </h5>
                <div class="card-body @yield('ticketid_content_parent_class')">
                    @yield('ticketid_content')
                </div>
            </div>
        @endif
        @yield('ticketid_extra_content')
    </div>
@stop
