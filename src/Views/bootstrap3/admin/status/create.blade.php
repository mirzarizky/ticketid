@extends($master)
@section('page', trans('ticketid::admin.status-create-title'))

@section('content')
    @include('ticketid::shared.header')
    <div class="well bs-component">
        {!! CollectiveForm::open(['route'=> $setting->grab('admin_route').'.status.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
            <legend>{{ trans('ticketid::admin.status-create-title') }}</legend>
            @include('ticketid::admin.status.form')
        {!! CollectiveForm::close() !!}
    </div>
@stop
