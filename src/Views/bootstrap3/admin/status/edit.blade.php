@extends($master)
@section('page', trans('ticketid::admin.status-edit-title', ['name' => ucwords($status->name)]))

@section('content')
    @include('ticketid::shared.header')
    <div class="well bs-component">
        {!! CollectiveForm::model($status, [
                                    'route' => [$setting->grab('admin_route').'.status.update', $status->id],
                                    'method' => 'PATCH',
                                    'class' => 'form-horizontal'
                                    ]) !!}
        <legend>{{ trans('ticketid::admin.status-edit-title', ['name' => ucwords($status->name)]) }}</legend>
        @include('ticketid::admin.status.form', ['update', true])
        {!! CollectiveForm::close() !!}
    </div>
@stop
