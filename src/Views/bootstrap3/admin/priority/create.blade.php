@extends($master)
@section('page', trans('ticketid::admin.priority-create-title'))

@section('content')
    @include('ticketid::shared.header')
    <div class="well bs-component">
        {!! CollectiveForm::open(['route'=> $setting->grab('admin_route').'.priority.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
            <legend>{{ trans('ticketid::admin.priority-create-title') }}</legend>
            @include('ticketid::admin.priority.form')
        {!! CollectiveForm::close() !!}
    </div>
@stop
