<div class="panel panel-default">
    <div class="panel-body">
        <ul class="nav nav-pills">
            <li role="presentation" class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\TicketsController@index')) ? "active" : "" !!}">
                <a href="{{ action('\Mirzarizky\Ticketid\Controllers\TicketsController@index') }}">{{ trans('ticketid::lang.nav-active-tickets') }}
                    <span class="badge">
                         <?php 
                            if ($u->isAdmin()) {
                                echo Mirzarizky\Ticketid\Models\Ticket::active()->count();
                            } elseif ($u->isAgent()) {
                                echo Mirzarizky\Ticketid\Models\Ticket::active()->agentUserTickets($u->id)->count();
                            } else {
                                echo Mirzarizky\Ticketid\Models\Ticket::userTickets($u->id)->active()->count();
                            }
                        ?>
                    </span>
                </a>
            </li>
            <li role="presentation" class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\TicketsController@indexComplete')) ? "active" : "" !!}">
                <a href="{{ action('\Mirzarizky\Ticketid\Controllers\TicketsController@indexComplete') }}">{{ trans('ticketid::lang.nav-completed-tickets') }}
                    <span class="badge">
                        <?php 
                            if ($u->isAdmin()) {
                                echo Mirzarizky\Ticketid\Models\Ticket::complete()->count();
                            } elseif ($u->isAgent()) {
                                echo Mirzarizky\Ticketid\Models\Ticket::complete()->agentUserTickets($u->id)->count();
                            } else {
                                echo Mirzarizky\Ticketid\Models\Ticket::userTickets($u->id)->complete()->count();
                            }
                        ?>
                    </span>
                </a>
            </li>

            @if($u->isAdmin())
                <li role="presentation" class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\DashboardController@index')) || Request::is($setting->grab('admin_route').'/indicator*') ? "active" : "" !!}">
                    <a href="{{ action('\Mirzarizky\Ticketid\Controllers\DashboardController@index') }}">{{ trans('ticketid::admin.nav-dashboard') }}</a>
                </li>

                <li role="presentation" class="dropdown {!!
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\StatusesController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\PrioritiesController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\AgentsController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\CategoriesController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\ConfigurationsController@index').'*') ||
                    $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\AdministratorsController@index').'*')
                    ? "active" : "" !!}">

                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        {{ trans('ticketid::admin.nav-settings') }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation" class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\StatusesController@index').'*') ? "active" : "" !!}">
                            <a href="{{ action('\Mirzarizky\Ticketid\Controllers\StatusesController@index') }}">{{ trans('ticketid::admin.nav-statuses') }}</a>
                        </li>
                        <li role="presentation"  class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\PrioritiesController@index').'*') ? "active" : "" !!}">
                            <a href="{{ action('\Mirzarizky\Ticketid\Controllers\PrioritiesController@index') }}">{{ trans('ticketid::admin.nav-priorities') }}</a>
                        </li>
                        <li role="presentation"  class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\AgentsController@index').'*') ? "active" : "" !!}">
                            <a href="{{ action('\Mirzarizky\Ticketid\Controllers\AgentsController@index') }}">{{ trans('ticketid::admin.nav-agents') }}</a>
                        </li>
                        <li role="presentation"  class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\CategoriesController@index').'*') ? "active" : "" !!}">
                            <a href="{{ action('\Mirzarizky\Ticketid\Controllers\CategoriesController@index') }}">{{ trans('ticketid::admin.nav-categories') }}</a>
                        </li>
                        <li role="presentation"  class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\ConfigurationsController@index').'*') ? "active" : "" !!}">
                            <a href="{{ action('\Mirzarizky\Ticketid\Controllers\ConfigurationsController@index') }}">{{ trans('ticketid::admin.nav-configuration') }}</a>
                        </li>
                        <li role="presentation"  class="{!! $tools->fullUrlIs(action('\Mirzarizky\Ticketid\Controllers\AdministratorsController@index').'*') ? "active" : "" !!}">
                            <a href="{{ action('\Mirzarizky\Ticketid\Controllers\AdministratorsController@index')}}">{{ trans('ticketid::admin.nav-administrator') }}</a>
                        </li>
                    </ul>
                </li>
            @endif

        </ul>
    </div>
</div>
