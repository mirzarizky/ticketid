@extends($master)

@section('page')
    {{ trans('ticketid::lang.index-title') }}
@stop

@section('content')
    @include('ticketid::shared.header')
    @include('ticketid::tickets.index')
@stop

@section('footer')
	<script src="//cdn.datatables.net/v/bs/dt-{{ Mirzarizky\Ticketid\Helpers\Cdn::DataTables }}/r-{{ Mirzarizky\Ticketid\Helpers\Cdn::DataTablesResponsive }}/datatables.min.js"></script>
	<script>
	    $('.table').DataTable({
	        processing: false,
	        serverSide: true,
	        responsive: true,
            pageLength: {{ $setting->grab('paginate_items') }},
        	lengthMenu: {{ json_encode($setting->grab('length_menu')) }},
	        ajax: '{!! route($setting->grab('main_route').'.data', $complete) !!}',
	        language: {
				decimal:        "{{ trans('ticketid::lang.table-decimal') }}",
				emptyTable:     "{{ trans('ticketid::lang.table-empty') }}",
				info:           "{{ trans('ticketid::lang.table-info') }}",
				infoEmpty:      "{{ trans('ticketid::lang.table-info-empty') }}",
				infoFiltered:   "{{ trans('ticketid::lang.table-info-filtered') }}",
				infoPostFix:    "{{ trans('ticketid::lang.table-info-postfix') }}",
				thousands:      "{{ trans('ticketid::lang.table-thousands') }}",
				lengthMenu:     "{{ trans('ticketid::lang.table-length-menu') }}",
				loadingRecords: "{{ trans('ticketid::lang.table-loading-results') }}",
				processing:     "{{ trans('ticketid::lang.table-processing') }}",
				search:         "{{ trans('ticketid::lang.table-search') }}",
				zeroRecords:    "{{ trans('ticketid::lang.table-zero-records') }}",
				paginate: {
					first:      "{{ trans('ticketid::lang.table-paginate-first') }}",
					last:       "{{ trans('ticketid::lang.table-paginate-last') }}",
					next:       "{{ trans('ticketid::lang.table-paginate-next') }}",
					previous:   "{{ trans('ticketid::lang.table-paginate-prev') }}"
				},
				aria: {
					sortAscending:  "{{ trans('ticketid::lang.table-aria-sort-asc') }}",
					sortDescending: "{{ trans('ticketid::lang.table-aria-sort-desc') }}"
				},
			},
	        columns: [
	            { data: 'id', name: 'ticketid.id' },
	            { data: 'subject', name: 'subject' },
	            { data: 'status', name: 'ticketid_statuses.name' },
	            { data: 'updated_at', name: 'ticketid.updated_at' },
            	{ data: 'agent', name: 'users.name' },
	            @if( $u->isAgent() || $u->isAdmin() )
		            { data: 'priority', name: 'ticketid_priorities.name' },
	            	{ data: 'owner', name: 'users.name' },
		            { data: 'category', name: 'ticketid_categories.name' }
	            @endif
	        ]
	    });
	</script>
@append
