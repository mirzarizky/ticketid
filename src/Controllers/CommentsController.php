<?php

namespace Mirzarizky\Ticketid\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mirzarizky\Ticketid\Models;
use Mirzarizky\Ticketid\Models\Setting;
use Mirzarizky\Ticketid\Models\Attachment;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('Mirzarizky\Ticketid\Middleware\IsAdminMiddleware', ['only' => ['edit', 'update', 'destroy']]);
        $this->middleware('Mirzarizky\Ticketid\Middleware\isAgentMiddleware', ['only' => ['pin', 'unpin']]);
        $this->middleware('Mirzarizky\Ticketid\Middleware\ResAccessMiddleware', ['only' => 'store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ticket_id'   => 'required|exists:ticketid,id',
            'content'     => 'required|min:6',
            'file_upload' => 'array|max:'.Setting::grab('max_upload_files').'',
            'file_upload.*' => 'bail|file|mimes:'.Setting::grab('allowed_file_types').'|max:'.Setting::grab('max_upload_file_size').''
        ]);

        $comment = new Models\Comment();

        $comment->setPurifiedContent($request->get('content'));

        $comment->ticket_id = $request->get('ticket_id');
        $comment->user_id = \Auth::user()->id;
        $comment->save();

        $ticket = Models\Ticket::find($comment->ticket_id);
        $ticket->updated_at = $comment->created_at;
        $ticket->save();
        if ($request->hasFile('file_upload')) {
            $files = $request->file('file_upload');
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $filename = $file->getFileName() . '.' . $extension;
                $filepath = '/app/attachments/' . $ticket->user_id . '/';
                $file->move(storage_path() . $filepath, $filename);

                $file_entry = new Attachment;
                $file_entry->mime = $file->getClientMimeType();
                $file_entry->original_filename = $file->getClientOriginalName();
                $file_entry->filename = $filename;
                $file_entry->filepath = $filepath;
                $file_entry->ticket_id = $comment->ticket->id;
                $file_entry->comment_id = $comment->id;
                $file_entry->save();
            }
        }

        return back()->with('status', trans('ticketid::lang.comment-has-been-added-ok'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Pin the specified comment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function pin($id)
    {
        $comment = Models\Comment::find($id);
        if (!$comment) {
            return abort(404);
        }
        if ($comment->isPinned) {
            return back()->with('warning', trans('ticketid::lang.comment-pinned'));
        } else {
            $comment->isPinned = true;
            $comment->save();
        }
        return back()->with('status', trans('ticketid::lang.comment-has-been-pinned'));
    }

    /**
     * Unpin the specified comment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function unpin($id)
    {
        $comment = Models\Comment::find($id);
        if (!$comment) {
            return abort(404);
        }
        if (!$comment->isPinned) {
            return back()->with('warning', trans('ticketid::lang.comment-unpinned'));
        } else {
            $comment->isPinned = false;
            $comment->save();
        }
        return back()->with('status', trans('ticketid::lang.comment-has-been-unpinned'));
    }
}