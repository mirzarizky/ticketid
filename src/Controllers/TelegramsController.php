<?php

namespace Mirzarizky\Ticketid\Controllers;

use App\Http\Controllers\Controller;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Mirzarizky\Ticketid\Models\Agent;
use Mirzarizky\Ticketid\Models\Configuration;
use Mirzarizky\Ticketid\Models\Setting;
use Mirzarizky\Ticketid\Models\Telegram;
use Telegram\Bot\Api as BOT;

class TelegramsController extends Controller
{
    protected $Telegram;

    public function __construct()
    {
        if (!empty(config('services.telegram-bot-api.token'))) {
            $this->Telegram = new BOT(config('services.telegram-bot-api.token'));
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $telegramSettings = Configuration::where('slug', 'telegram_notification')->first();
        $webhookStatus = $this->Telegram->getWebhookInfo();
        $telegramUsers = Agent::whereNotNull('telegram_id')->get();
        $botUsername = Configuration::where('slug', 'telegram_bot_username')->first();

        return view('ticketid::admin.telegram.index', compact('telegramUsers', 'telegramSettings', 'webhookStatus', 'botUsername'));
    }

    public function indexAgent()
    {
        $user = Agent::find(\Auth::user()->id);
        return view('ticketid::agent.telegram.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function showMenu($chatid, $info = null){
        $message = '';
        if($info !== null){
            $message .= $info.chr(10);
        }
        $message .=  '/website'.chr(10);
        $message .= '/contact'.chr(10);

        $response = $this->Telegram->sendMessage([
            'chat_id' => $chatid,
            'text' => $message
        ]);
    }

    public function showWebsite($chatid){
        $message = 'https://mirzarizky.com';

        $response = $this->Telegram->sendMessage([
            'chat_id' => $chatid,
            'text' => $message
        ]);
    }

    public function showContact($chatid){
        $message = 'hello@mirzarizky.com';

        $response = $this->Telegram->sendMessage([
            'chat_id' => $chatid,
            'text' => $message
        ]);
    }

    public function activate($chatid)
    {
        $telegramUser = Telegram::where('chat_id', $chatid)->first();
        if (empty($telegramUser)) {
            $message = 'Your Secret PIN is '.$chatid;
        } else {
            $message = 'Your Telegram already activated.';
        }

        $response = $this->Telegram->sendMessage([
            'chat_id' => $chatid,
            'text' => $message
        ]);
    }
    public function deactivate(Request $request)
    {
        $request->validate([
            'username' => 'string|required'
        ]);
        $agent = Agent::find(\Auth::user()->id);
        if($agent->telegram->username == $request->username) {
            $message = 'Hi '.$request->username.'! Your Telegram Notification has been deactivated.';
            $response = $this->Telegram->sendMessage([
                'chat_id' => $agent->telegram->chat_id,
                'text' => $message
            ]);
            $agent->telegram_id = null;
            $agent->telegram->delete();
            $agent->save();
        } else {
            return redirect()->back()->with('warning', 'Your username not match.');
        }
        return redirect()->route(Setting::grab('main_route').'.agent.telegram.index')->with('status', 'Your Telegram has been deactivated.');
    }

    public function activation(Request $request)
    {
        $request->validate([
            'username' => 'string|required',
            'secret_pin' => 'numeric|required'
        ]);
        $agent = Agent::find(\Auth::user()->id);
        $telegram = new Telegram();
        $telegram->user_id = $agent->id;
        $telegram->username = $request->username;
        $telegram->chat_id = $request->secret_pin;
        $telegram->save();
        $agent->telegram_id = $telegram->id;
        $agent->save();
        $message = 'Congratulations '.$telegram->username.'! Your Telegram Notification has been activated.';
        $response = $this->Telegram->sendMessage([
            'chat_id' => $telegram->chat_id,
            'text' => $message
        ]);
        return redirect()->route(Setting::grab('main_route').'.agent.telegram.index')->with('status', 'Your Telegram has been activated.');
    }

    public function setWebHook(Request $request)
    {
        $request->validate([
            'url' => 'bail|required|url',
        ]);
        $url = $request->url;
        $response = $this->Telegram->setWebhook(['url' => ''.$url.'/'.config('services.telegram-bot-api.token').'/webhook']);
        if($response) {
            return redirect()->route(Setting::grab('admin_route').'.telegram.index');
        } else {
            return $response['description'];
        }
    }

    public function removeWebHook()
    {
        $response = $this->Telegram->removeWebhook();

        return redirect()->route(Setting::grab('admin_route').'.telegram.index');
    }

    public function webhook(Request $request){
        $chatid = $request['message']['chat']['id'];
        $text = $request['message']['text'];

        switch($text) {
//            case '/start':
//                $this->showMenu($chatid);
//                break;
//            case '/menu':
//                $this->showMenu($chatid);
//                break;
            case '/website':
                $this->showWebsite($chatid);
                break;
            case '/contact';
                $this->showContact($chatid);
                break;
            case '/activate':
                $this->activate($chatid);
                break;
//            default:
//                $info = 'I do not understand what you just said. Please choose an option';
//                $this->showMenu($chatid, $info);
        }
    }
}
